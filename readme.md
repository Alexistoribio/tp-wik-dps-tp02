Guide de Démarrage du Serveur
=============================

Ce document fournit des instructions étape par étape pour démarrer un serveur en utilisant Node.js, ainsi que pour configurer et utiliser Docker.

Prérequis
---------

*   [Visual Studio Code (VS Code)](https://code.visualstudio.com/)
*   [Node.js et npm](https://nodejs.org/)
*   [Docker Desktop](https://www.docker.com/products/docker-desktop)

Démarrage du Serveur Node.js
----------------------------

### 1\. Compilation du Code TypeScript

Ouvrez le terminal dans VS Code et exécutez la commande suivante pour compiler le code TypeScript :

bash

```bash
npx tsc
```

### 2\. Configuration du Port

Dans le même terminal, configurez le port d'écoute du serveur :

bash

```bash
set PING_LISTEN_PORT=8000
```

### 3\. Lancement du Serveur

Maintenant, démarrez le serveur avec la commande suivante :

bash

```bash
node build/index.js
```

### 4\. Vérification dans le Navigateur

Ouvrez votre navigateur et saisissez l'URL suivante pour vérifier que le serveur fonctionne correctement :

bash

```bash
http://localhost:8000/ping
```

Si une erreur 404 s'affiche, assurez-vous d'avoir ajouté "/ping" à la fin de l'URL.

Utilisation de Docker
---------------------

### Installation et Configuration

1.  Installez Docker Desktop et lancez-le.
2.  Installez l'extension Docker dans VS Code.

### Lancement du Serveur avec Docker

1.  Ouvrez un terminal dans VS Code et construisez l'image Docker avec la commande suivante :
    
    bash
    
    ```bash
    docker build -t test-node .
    ```
    
2.  Une fois le build terminé, lancez le conteneur Docker :
    
    bash
    
    ```bash
    docker run -it --rm -p 8080:8000 -e PING_LISTEN_PORT=8000 test-node
    ```
    
    Cette commande démarre le serveur local qui écoutera sur le port 8080.
    

### Vérification avec curl

Ouvrez un autre terminal et exécutez la commande suivante pour vous assurer que la page web est fonctionnelle :

bash

```bash
curl http://localhost:8080/ping -v
```

Processus en Deux Étapes (Double Stages)
----------------------------------------

### Construction de l'Image avec les Deux Étapes

Dans un nouveau terminal, exécutez :

bash

```bash
docker build -t 2e-image -f Dockerfile .
```

### Lancement et Vérification

Répétez les étapes de lancement du serveur avec Docker en changeant les ports :

bash

```bash
docker run -it --rm -p 8081:8000 -e PING_LISTEN_PORT=8000 test-node
```

Vérifiez avec curl, en ajustant le port :

bash

```bash
curl http://localhost:8081/ping -v
```

Vous devriez à présent être connecté au serveur.
